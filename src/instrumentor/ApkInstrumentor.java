package instrumentor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import org.apache.commons.io.FileUtils;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.util.CheckClassAdapter;

public class ApkInstrumentor {

	private static boolean allInputsObtained = true;

	private static String pathToApkClassFiles = null;
	private static String pathToDedexed = null;

	private static List<String> whitelist = new ArrayList<String>();

	public static void instrument(String apkFilePath) {

		// make the output-folder and the temp folder
		File fp;
		try {
			fp = new File(Global.pathToOutputFolder);
			if (fp.exists() && fp.isDirectory()) {
				InstrumentPanel.jTextArea1.append("Cleaning output directory " + Global.pathToOutputFolder + " \n");
				FileUtils.cleanDirectory(fp);
			} else {
				InstrumentPanel.jTextArea1.append("Cleaning output directory " + Global.pathToOutputFolder + " \n");
				FileUtils.forceMkdir(fp);
			}
		} catch (final IOException e1) {
			InstrumentPanel.jTextArea1.append("Error \n");
			e1.printStackTrace();
		}

		// Read whitelist of APIs to instrument and store them in a list

		try {
			final BufferedReader br = new BufferedReader(new FileReader(Global.default_api_collection));

			String line;
			while ((line = br.readLine()) != null) {
				if (line.isEmpty() || line.charAt(0) == '#') {
					continue;
				}

				whitelist.add(line);
			}

			br.close();
		} catch (final FileNotFoundException e) {
			InstrumentPanel.jTextArea1.append("Error \n");
			e.printStackTrace();
		} catch (final IOException e) {
			InstrumentPanel.jTextArea1.append("Error \n");
			e.printStackTrace();
		}

		final String commandToDedex = "d2j-dex2jar.bat " + apkFilePath + " -f -o " + Global.pathToOutputFolder
				+ File.separator + "dedexed.jar";

		final Process p;
		try {
			InstrumentPanel.jTextArea1.append("Executing command : " + commandToDedex);
			InstrumentPanel.jTextArea1.append("\n");
			p = Runtime.getRuntime().exec(commandToDedex);
			final int err = p.waitFor();
		} catch (final IOException e) {
			InstrumentPanel.jTextArea1.append("Error \n");
			e.printStackTrace();
		} catch (final InterruptedException e) {
			InstrumentPanel.jTextArea1.append("Error \n");
			e.printStackTrace();
		}

		File files[];
		files = new File(Global.pathToOutputFolder).listFiles();
		Arrays.sort(files);
		for (int i = 0, n = files.length; i < n; i++) {
			if (pathToDedexed != null) {
				break;
			}

			if (!files[i].isDirectory()) {
				if (files[i].toString().endsWith("dedexed.jar")) {
					pathToDedexed = files[i].toString();
				}
			}
		}
		if (pathToDedexed == null) {
			InstrumentPanel.jTextArea1.append("dex2jar output not found");
			InstrumentPanel.jTextArea1.append("\n");
			allInputsObtained = false;
		} else {
			InstrumentPanel.jTextArea1.append("dex2jar output generated in the output-folder");
			InstrumentPanel.jTextArea1.append("\n");
		}

		// if dedexed has been generated in the output folder extract the class
		// files from it
		if (allInputsObtained) {
			unzip();
		}

		// check if the class files have been successfully generated in the
		// output-folder
		files = new File(Global.pathToOutputFolder).listFiles();
		Arrays.sort(files);
		if (files.length <= 1) {
			allInputsObtained = false;
			InstrumentPanel.jTextArea1.append("Unzipping did not go well, cannot find the class files for the apk");
			InstrumentPanel.jTextArea1.append("\n");

		} else {

			InstrumentPanel.jTextArea1.append("Recording app classed in classTable...");
			InstrumentPanel.jTextArea1.append("\n");
			pathToApkClassFiles = null;
			for (final File file : files) {
				if (pathToApkClassFiles != null) {
					break;
				}

				if (file.isDirectory()) {
					pathToApkClassFiles = file.toString();
					InstrumentPanel.jTextArea1.append("Class files generated in " + pathToApkClassFiles);
					InstrumentPanel.jTextArea1.append("\n");
					final File dir = new File(pathToApkClassFiles);
					recordDirectory(dir);
				}
			}

		}

		repackage(apkFilePath);
	}

	public static void unzip() {
		final String source = pathToDedexed;
		final String destination = Global.pathToOutputFolder + File.separator + "extracted";

		try {
			final ZipFile zipFile = new ZipFile(source);
			zipFile.extractAll(destination);
		} catch (final ZipException e) {
			e.printStackTrace();
		}
	}

	public static void zip() {
		final String source = Global.pathToOutputFolder + File.separator + "extracted" + File.separator;
		final String destination = Global.pathToOutputFolder + File.separator + "instrumented.jar";

		try {
			// Initiate ZipFile object with the path/name of the zip file.
			final ZipFile zipFile = new ZipFile(destination);

			// Initiate Zip Parameters which define various properties such
			// as compression method, etc.
			final ZipParameters parameters = new ZipParameters();

			// set compression method to store compression
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);

			// Set the compression level. This value has to be in between 0 to 9
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);

			parameters.setIncludeRootFolder(false);

			zipFile.addFolder(source, parameters);
		} catch (final ZipException e) {
			e.printStackTrace();
		}
	}

	public static void addFileInZip(String fileToAdd) {
		final String source = Global.pathToOutputFolder + File.separator + "instrumented.apk";

		try {
			final ZipFile zipFile = new ZipFile(source);

			final ZipParameters parameters = new ZipParameters();
			parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
			parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);

			zipFile.addFile(new File(fileToAdd), parameters);
		} catch (final ZipException e) {
			e.printStackTrace();

		}
	}

	public static void displayDirectory(File dir) {

		// InstrumentPanel.jTextArea1.append(dir.getAbsoluteFile());

		if (dir.isDirectory()) {
			final String[] subNote = dir.list();
			for (final String filename : subNote) {
				displayDirectory(new File(dir, filename));
			}
		}
	}

	public static void repackage(String apkFilePath) {
		// zip all class files to jar
		zip();

		// obtain classes.dex from jar
		// String command = "bash "+Global.pathToDex2Jar +
		// "/d2j-jar2dex.sh -f -o " + Global.pathToOutputFolder +
		// "/classes.dex "
		// + Global.pathToOutputFolder + "/instrumented.jar";
		String command = "d2j-jar2dex.bat -f -o " + Global.pathToOutputFolder + File.separator + "classes.dex "
				+ Global.pathToOutputFolder + File.separator + "instrumented.jar";

		Process p;
		try {
			InstrumentPanel.jTextArea1.append("Executing command : " + command);
			InstrumentPanel.jTextArea1.append("\n");
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// replace instrumented classes.dex in a copy of the original APK
		command = "cp " + apkFilePath + " " + Global.pathToOutputFolder + File.separator + "instrumented.apk";

		try {
			InstrumentPanel.jTextArea1.append("Executing command : " + command);
			InstrumentPanel.jTextArea1.append("\n");
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		addFileInZip(Global.pathToOutputFolder + File.separator + "classes.dex");

		// sign and rename instrumented APK
		// command = "bash "+ Global.pathToDex2Jar + "/d2j-apk-sign.sh -f -o " +
		// Global.pathToOutputFolder
		// + "/signed-instrumented.apk " + Global.pathToOutputFolder +
		// "/instrumented.apk";
		command = "d2j-apk-sign.bat -f -o " + Global.pathToOutputFolder + "/signed-instrumented.apk "
				+ Global.pathToOutputFolder + File.separator + "instrumented.apk";

		try {
			InstrumentPanel.jTextArea1.append("Executing command : " + command);
			InstrumentPanel.jTextArea1.append("\n");
			p = Runtime.getRuntime().exec(command);
			p.waitFor();
		} catch (final Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// InstrumentPanel.jTextArea1.append("'signed-instrumented.apk' created in output folder!");
		// InstrumentPanel.jTextArea1.append("\n");
	}

	public static void recordDirectory(File dir) {

		final String name = dir.getName();

		if (dir.isDirectory()) {

			final String[] subNote = dir.list();
			for (final String filename : subNote) {
				recordDirectory(new File(dir, filename));
			}
		} else {
			// if class file record it in the hash table
			if (name.endsWith(".class")) {
				parseClass(name, dir.getAbsolutePath());
			}
		}
	}

	public static void parseClass(String nameOfClass, String pathToClassFile) {

		final String instrumentedClassPath = pathToClassFile;
		final String instrumentedDirPath = instrumentedClassPath.substring(0, pathToClassFile.lastIndexOf("\\"));

		final File theFile = new File(instrumentedDirPath);
		theFile.mkdirs();

		try {

			final InputStream in = new FileInputStream(pathToClassFile);
			final ClassReader cr = new ClassReader(in);

			final ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);

			CheckClassAdapter visitor = null;
			if (Global.efgExtractMode == 0) {
				visitor = new APICallInstrumentor(writer, whitelist, nameOfClass.substring(0,
						nameOfClass.lastIndexOf(".")));
			} else {
				visitor = new EventHandlerInstrumentor(writer, pathToClassFile.substring(
						pathToClassFile.indexOf("extracted/") + 10, pathToClassFile.lastIndexOf(".")).replace('/', '.'));
			}

			cr.accept(visitor, 0);
			final byte[] instrumentedBytecode = writer.toByteArray();

			final OutputStream fos = new FileOutputStream(instrumentedClassPath);
			fos.write(instrumentedBytecode);
			fos.flush();
			fos.close();

		} catch (final Exception e) {
			InstrumentPanel.jTextArea1.append("Error \n");
			e.printStackTrace();
		}
	}

}
