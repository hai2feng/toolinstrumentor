package instrumentor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.StringTokenizer;

//import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.commons.InstructionAdapter;
import org.objectweb.asm.util.CheckClassAdapter;

public class APICallInstrumentor extends CheckClassAdapter implements Opcodes {
	String className;
	String classPath;
	List<String> whitelist;
	Queue<Object> queue = Collections.asLifoQueue(new LinkedList<Object>());

	int count = 0;

	public APICallInstrumentor(ClassVisitor arg0, List<String> whitelist, String className) {
		super(arg0);
		this.className = className;
		this.whitelist = whitelist;
	}

	@Override
	public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
		classPath = name;
		super.visit(version, access, name, signature, superName, interfaces);
	}

	@Override
	public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {

		final MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);

		final InstructionAdapter instMv = new InstructionAdapter(mv) {

			@Override
			public void visitInsn(int opcode) {
				mv.visitInsn(opcode);
			}

			@Override
			public void visitLdcInsn(Object cst) {
				queue.add(cst);
				mv.visitLdcInsn(cst);
			}

			@Override
			public void visitMethodInsn(int opcode, String owner, String name, String desc) {
				final String compare = "L" + owner + ";->" + name;

				for (final String line : whitelist) {
					if (line.isEmpty() || line.charAt(0) == '#') {
						continue;
					}

					if (className.contains("DefaultUtil") && compare.contains("apache") && line.contains("apache")) {
						System.err.println("line " + line + " owner " + owner);
					}

					// if (line.equals(compare)){
					if (line.contains(owner) && !name.contains("toString") && !name.contains("<init>")) {
						// if (compare.contains(line) &&
						// !name.contains("com.google.ads.internal")
						// && !name.contains("com.google.ads.util")
						// && !name.contains("com.google.ads.aq")) {
						InstrumentPanel.jTextArea1.append("Instrumented: " + compare);
						InstrumentPanel.jTextArea1.append("\n");

						if (name.contains("requestLocationUpdates") || name.contains("registerListener")
								|| name.contains("setFlashMode") || name.contains("acquire")) {

							final List<String> params = new ArrayList<String>();
							final List<String> semicolons = new ArrayList<String>();

							final int idx1 = desc.indexOf("(");
							final int idx2 = desc.indexOf(")");
							if (idx1 == -1 || idx2 == -1) {
								System.out.println("parsing error, disable this part");
							}
							final String temp = desc.substring(idx1 + 1, idx2);
							final StringTokenizer st = new StringTokenizer(temp, ";");

							while (st.hasMoreTokens()) {
								semicolons.add(st.nextToken());
							}

							System.out.println(semicolons);

							for (final String s : semicolons) {

								final char[] car = s.toCharArray();
								for (int i = 0; i < car.length; i++) {
									if (car[i] == 'L') {
										params.add(s.substring(i, s.length()));
										break;
									} else {
										params.add(car[i] + "");
									}
								}
							}

							// Collections.reverse(params);
							for (final String p : params) {

								if (queue.size() > 0) {

									final Object toAppend = queue.remove();
									if (toAppend instanceof Long) {
										mv.visitLdcInsn("ResourceParameters");
										mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
										mv.visitInsn(DUP);
										mv.visitLdcInsn(compare + desc + "=Long=");
										mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>",
												"(Ljava/lang/String;)V");
										mv.visitLdcInsn(toAppend);
										final String type = "(J)Ljava/lang/StringBuilder;";
										mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", type);
										mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString",
												"()Ljava/lang/String;");

										mv.visitMethodInsn(INVOKESTATIC, "android/util/Log", "i",
												"(Ljava/lang/String;Ljava/lang/String;)I");
										mv.visitInsn(POP);
									} else if (toAppend instanceof Float) {
										mv.visitLdcInsn("ResourceParameters");
										mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
										mv.visitInsn(DUP);
										mv.visitLdcInsn(compare + desc + "=Float=");
										mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>",
												"(Ljava/lang/String;)V");
										mv.visitLdcInsn(toAppend);
										final String type = "(F)Ljava/lang/StringBuilder;";
										mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", type);
										mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString",
												"()Ljava/lang/String;");

										mv.visitMethodInsn(INVOKESTATIC, "android/util/Log", "i",
												"(Ljava/lang/String;Ljava/lang/String;)I");
										mv.visitInsn(POP);
									} else if (toAppend instanceof Integer) {
										mv.visitLdcInsn("ResourceParameters");
										mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
										mv.visitInsn(DUP);
										mv.visitLdcInsn(compare + desc + "=Integer=");
										mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>",
												"(Ljava/lang/String;)V");
										mv.visitLdcInsn(toAppend);
										final String type = "(I)Ljava/lang/StringBuilder;";
										mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", type);
										mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString",
												"()Ljava/lang/String;");

										mv.visitMethodInsn(INVOKESTATIC, "android/util/Log", "i",
												"(Ljava/lang/String;Ljava/lang/String;)I");
										mv.visitInsn(POP);
									} else if (toAppend instanceof Double) {
										mv.visitLdcInsn("ResourceParameters");
										mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
										mv.visitInsn(DUP);
										mv.visitLdcInsn(compare + desc + "=Double=");
										mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>",
												"(Ljava/lang/String;)V");
										mv.visitLdcInsn(toAppend);
										final String type = "(D)Ljava/lang/StringBuilder;";
										mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", type);
										mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString",
												"()Ljava/lang/String;");

										mv.visitMethodInsn(INVOKESTATIC, "android/util/Log", "i",
												"(Ljava/lang/String;Ljava/lang/String;)I");
										mv.visitInsn(POP);
									}

								}
							}
							InstrumentPanel.jTextArea1.append(name + ": name : " + desc);
							InstrumentPanel.jTextArea1.append("\n");

						}

						// Instrument the method by calling Android's log
						// function
						mv.visitLdcInsn("DroidBox");

						/** alternative instrumentation with tid ***/
						// mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
						// mv.visitInsn(DUP);
						// mv.visitMethodInsn(INVOKESPECIAL,
						// "java/lang/StringBuilder", "<init>", "()V");
						// mv.visitLdcInsn(compare + desc + "="
						// // classPath + "->" + name + desc
						// );
						// mv.visitMethodInsn(INVOKEVIRTUAL,
						// "java/lang/StringBuilder", "append",
						// "(Ljava/lang/String;)Ljava/lang/StringBuilder;");
						// mv.visitMethodInsn(INVOKESTATIC,
						// "android/os/Process", "myTid", "()I");
						// mv.visitMethodInsn(INVOKEVIRTUAL,
						// "java/lang/StringBuilder", "append",
						// "(I)Ljava/lang/StringBuilder;");
						// mv.visitMethodInsn(INVOKEVIRTUAL,
						// "java/lang/StringBuilder", "toString",
						// "()Ljava/lang/String;");

						/** old instrumentation without tid ***/

						mv.visitLdcInsn(compare + desc
						// classPath + "->" + name + desc
						);
						mv.visitMethodInsn(INVOKESTATIC, "android/util/Log", "v",
								"(Ljava/lang/String;Ljava/lang/String;)I");
						mv.visitInsn(POP);

						count++;

						break;
					}
				}

				mv.visitMethodInsn(opcode, owner, name, desc);

			}

		};

		return instMv;
	}
}
