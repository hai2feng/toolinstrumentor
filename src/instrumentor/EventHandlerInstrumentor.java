package instrumentor;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.util.CheckClassAdapter;

public class EventHandlerInstrumentor extends CheckClassAdapter implements Opcodes {
	String className;

	public EventHandlerInstrumentor(ClassVisitor arg0, String className) {
		super(arg0);
		this.className = className;
		// TODO Auto-generated constructor stub
	}

	@Override
	public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
		// delegate the method call to the next
		// chained visitor
		MethodVisitor mv = cv.visitMethod(access, name, desc, signature, exceptions);
		// only instrument non-abstract methods

		// if((access & ACC_ABSTRACT) == 0
		// && name.startsWith("on")
		// && !className.equals("RemoteControlClientCompat")) {
		if ((access & ACC_ABSTRACT) == 0
				// && !name.contains("<init>")
				&& !name.contains("<cinit>") && !name.contains("access$") && !name.contains("<clinit>")
				&& !name.contains("com.google.ads.internal") && !name.contains("com.google.ads.util")
				&& !name.contains("com.google.ads.aq") && !className.equals("RemoteControlClientCompat")) {

			mv.visitLdcInsn("EventHandler");

			/** alternative instrumentation with tid ***/
			mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
			mv.visitInsn(DUP);
			mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V");
			mv.visitLdcInsn(className + "_" + name + desc + "=");
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append",
					"(Ljava/lang/String;)Ljava/lang/StringBuilder;");
			mv.visitMethodInsn(INVOKESTATIC, "android/os/Process", "myTid", "()I");
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;");
			mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");

			/** old instrumentation without tid ***/

			// mv.visitLdcInsn(className + "_" + name + desc);

			/************************************/
			mv.visitMethodInsn(INVOKESTATIC, "android/util/Log", "i", "(Ljava/lang/String;Ljava/lang/String;)I");
			mv.visitInsn(POP);
			InstrumentPanel.jTextArea1.append("instrumenting : " + name);
			InstrumentPanel.jTextArea1.append("\n");
			ModifiedAdviceAdapter.className = className;

			mv = new ModifiedAdviceAdapter(Opcodes.ASM4, access, name, desc, mv);

		} else {
			// InstrumentPanel.jTextArea1.append("not instrumenting : " + name);

		}
		return mv;
	}
}
